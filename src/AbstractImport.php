<?php

namespace Neneff\Import;


use Neneff\Tools\Log;

abstract class AbstractImport
{
    /** @var \PDO */
    protected $_pdo = null;

    /** @var array */
    private $_logs = [];

    /** @var String */
    protected $_memoryLimit = '4092M';

    /** @var String */
    protected $_maxExecutionTime = '1200';

    /** @var array */
    protected $_data = [];


    /**
     * AbstractImportData constructor.
     * @param \PDO $pdo
     */
    public function __construct(\PDO $pdo)
    {
        ini_set('memory_limit'      , $this->_memoryLimit);
        ini_set('max_execution_time', $this->_maxExecutionTime);

        $this->_pdo = $pdo;
    }

    /**
     * @param String $type
     * @param String $message
     * @param array  $data   <p>array of string</p>
     *
     * @return array <p>the new created log</p>
     */
    public function addLog($type, $message, $data = null)
    {
        $log = Log::make($type, $message, $data);
        $this->_logs[] = $log;

        return $log;
    }

    /**
     * @param array $logs
     */
    public function addLogs(array $logs)
    {
        $this->_logs = array_merge($this->_logs, $logs);
    }

    /**
     * @return array
     */
    public function getLogs()
    {
        return $this->_logs;
    }

}