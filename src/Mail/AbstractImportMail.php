<?php

namespace Neneff\Import\Mail;


use Neneff\Import\AbstractImport;
use Neneff\Tools\Date;
use Neneff\Tools\Log;
use Neneff\Tools\Sql;
use PhpImap\IncomingMail;
use PhpImap\IncomingMailAttachment;
use PhpImap\Mailbox;

abstract class AbstractImportMail extends AbstractImport
{
    static $PROCESS_STATUS_INIT      = 'INIT';       // -- TEMP
    static $PROCESS_STATUS_IGNORED   = 'IGNORED';    // -- FINAL
    static $PROCESS_STATUS_COMPLETE  = 'COMPLETE';   // -- FINAL
    static $PROCESS_STATUS_ERROR     = 'ERROR';      // -- FINAL


    /** @var Mailbox */
    protected $_mailClient = null;

    /** @var int[]  <p>List of all mails that match the search</p> */
    protected $_mailIds = [];

    /** @var int[]  <p>List of all mails that are not yet in log table</p> */
    protected $_newMailIds = [];

    /** @var int[]  <p>List of all mails that are already in the log table</p> */
    protected $_duplicatesMailIds = [];

    /** @var  string <p>Provide a table name to log this import</p> */
    protected $_logDbTable = '';

    /** @var int <p>maximum of mail to process at once</p> */
    protected $_bulkLimit = 100;


    /**
     * AbstractImportMail constructor, Will search for mail and will performs all the processes
     * @param \PDO   $pdo
     * @param array  $mailboxConfiguration <p>should contains 'imappath', 'login', 'password', 'attachmtdir', 'encoding' keys</p>
     *
     * @throws \Exception
     */
    public function __construct(\PDO $pdo, $mailboxConfiguration)
    {
        parent::__construct($pdo);

        // -- generate the log table
        $this->_pdo->exec("
            CREATE TABLE IF NOT EXISTS {$this->_logDbTable} (
                id               INT(11) NOT NULL AUTO_INCREMENT,
                mail_id          INT(11) DEFAULT NULL,
                mail_subject     VARCHAR(255),
                mail_date        DATETIME,
                mail_content     LONGTEXT,  
                mail_attachments LONGTEXT,
                process_status   VARCHAR(255),  
                import_logs      LONGTEXT, 
                created_at       DATETIME, 
                updated_at       DATETIME, 
                PRIMARY KEY (id),
                UNIQUE KEY `MAIL_ID_IDX` (mail_id)
            ) ENGINE=innoDB DEFAULT CHARSET=utf8;
        ");

        $this->_mailClient = new Mailbox(
            $mailboxConfiguration['imappath'],
            $mailboxConfiguration['login'],
            $mailboxConfiguration['password'],
            $mailboxConfiguration['attachmtdir'],
            $mailboxConfiguration['encoding']
        );

        // -- Duplicates are bad
        $this->addLog(Log::$LOG_MESSAGE, "Fetching Mailbox <{$mailboxConfiguration['login']}> with criteria {$this->_getSearchCriteria()}");

        // -- retrieve Unseen mails list we are supposed to import
        $this->_mailIds = $this->_mailClient->searchMailBox($this->_getSearchCriteria());

        if(count($this->_mailIds) > 0)
        {
            $count = 0;

            // -- retrieve list of mail already parsed
            $placeholders = Sql::preparePlaceholdersList(count($this->_mailIds), count($this->_mailIds));
            $stmt         = $pdo->prepare("SELECT mail_id FROM {$this->_logDbTable} WHERE mail_id IN {$placeholders}");
            $stmt->execute($this->_mailIds);

            $this->_duplicatesMailIds = $stmt->fetchAll(\PDO::FETCH_COLUMN);
            $this->_newMailIds        = array_diff($this->_mailIds, $this->_duplicatesMailIds);

            // -- new mail to be proceed
            $countNewMail = count($this->_newMailIds);
            $this->addLog(Log::$LOG_MESSAGE, "{$countNewMail} new Mail(s) found");

            // -- If Duplicates are bad, warning it
            if($this->_warnForDuplicate() && count($this->_duplicatesMailIds) > 0)
            {
                $this->addLog(Log::$LOG_WARNING, 'There are '.count($this->_duplicatesMailIds).' mail(s) that are already been parsed', [
                    'duplicates' => join(', ', $this->_duplicatesMailIds)
                ]);
            }

            // -- Parse new Mails
            foreach($this->_newMailIds as $mailId)
            {
                $this->addLogForMail($mailId, Log::$LOG_MESSAGE, 'Parsing mail '.$mailId);

                $mail        = $this->_mailClient->getMail($mailId);
                $attachments = array_map(function(IncomingMailAttachment $obj) {
                    return [
                        'id'   => $obj->id,
                        'name' => $obj->name,
                        'path' => $obj->filePath
                    ];
                }, $mail->getAttachments());
                $stmt = $pdo->prepare("
                    INSERT INTO {$this->_logDbTable}( mail_id,  mail_content,  mail_subject,  mail_date,  mail_attachments,  process_status,  updated_at,  created_at)
                    VALUES                          (:mail_id, :mail_content, :mail_subject, :mail_date, :mail_attachments, :process_status, :updated_at, :created_at)
                ");
                $stmt->execute([
                    ':mail_id'          => $mailId,
                    ':mail_subject'     => $mail->subject,
                    ':mail_date'        => $mail->date,
                    ':mail_content'     => $mail->textPlain,
                    ':mail_attachments' => json_encode($attachments),
                    ':process_status'   => self::$PROCESS_STATUS_INIT,
                    ':updated_at'       => Date::getCurrentDateTimeUTC(),
                    ':created_at'       => Date::getCurrentDateTimeUTC(),
                ]);

                // -- process the data here
                if($this->_mailShouldBeProcessed($mail))
                {
                    $count ++;
                    try
                    {
                        $this->_data[$mailId] = $this->_importMail($mail);

                        // -- mark the mail as complete
                        $stmt = $pdo->prepare("
                            UPDATE {$this->_logDbTable} SET
                              process_status = :process_status,
                              import_logs    = :import_logs,
                              updated_at     = :updated_at
                            WHERE mail_id = :mail_id
                        ");
                        $stmt->execute([
                            ':process_status' => self::$PROCESS_STATUS_COMPLETE,
                            ':updated_at' => Date::getCurrentDateTimeUTC(),
                            ':import_logs' => json_encode($this->getLogsForMail($mailId)),
                            ':mail_id' => $mailId
                        ]);
                    }
                    catch(\Exception $e)
                    {
                        $this->addLogForMail($mailId, Log::$LOG_ERROR, 'Error when parsing mail '.$mailId, [
                            '$exception' => [
                                'code'    => $e->getCode(),
                                'message' => $e->getMessage(),
                                'file'    => $e->getFile(),
                                'line'    => $e->getLine(),
                                'trace'   => $e->getTraceAsString(),
                            ]
                        ]);

                        // -- mark the mail as error and log the error
                        $stmt = $pdo->prepare("
                            UPDATE {$this->_logDbTable} SET
                              process_status = :process_status,
                              import_logs    = :import_logs,
                              updated_at     = :updated_at
                            WHERE mail_id = :mail_id
                        ");
                        $stmt->execute([
                            ':mail_id'        => $mailId,
                            ':process_status' => self::$PROCESS_STATUS_ERROR,
                            ':import_logs'    => json_encode($this->getLogsForMail($mailId)),
                            ':updated_at'     => Date::getCurrentDateTimeUTC(),
                        ]);
                    }

                    // -- When we reach the limit stop process
                    if($count >= $this->_bulkLimit) {
                        break;
                    }
                }
                else
                {
                    $stmt = $pdo->prepare("
                        UPDATE {$this->_logDbTable} SET
                          process_status = :process_status,
                          updated_at     = :updated_at
                        WHERE mail_id = :mail_id
                    ");
                    $stmt->execute([
                        ':mail_id'        => $mailId,
                        ':process_status' => self::$PROCESS_STATUS_IGNORED,
                        ':updated_at'     => Date::getCurrentDateTimeUTC(),
                    ]);
                }
                $this->_mailPostProcess($mail);
            }
        }
        else
        {
            $this->addLog(Log::$LOG_MESSAGE , 'No Mail Found with the following criteria :: '.$this->_getSearchCriteria());
        }
    }


    /**
     * Criteria used to search for mails, could be the subject
     * @return string
     */
    protected function _getSearchCriteria()
    {
        return 'UNSEEN';
    }


    /**
     * Execute the import
     * You should import your data into you tool from here
     * @param IncomingMail $mail
     */
    abstract protected function _importMail(IncomingMail $mail);


    /**
     * Once a mail is extracted and imported
     * @param IncomingMail $mail
     */
    public function _mailPostProcess(IncomingMail $mail)
    {
        imap_setflag_full($this->_mailClient->getImapStream(), (string)$mail->id, '\Flagged', ST_UID);
    }

    /**
     * do we warn when there is duplicate ?
     * @return bool
     */
    protected function _warnForDuplicate()
    {
        return true;
    }

    /**
     * <p> USe this if you want to filter mail (with the title for instance or only the last one) </p>
     * @param IncomingMail $mail
     * @return Boolean
     */
    protected function _mailShouldBeProcessed(IncomingMail $mail)
    {
        return true;
    }


    /**
     * Mark a log for a specific mail
     * @param integer $mailId
     * @param string  $type
     * @param string  $message
     * @param array   $data
     */
    public function addLogForMail($mailId, $type, $message, array $data = [])
    {
        if(!$data) {
            $data = [];
        }
        $data['$mail'] = $mailId;

        return $this->addLog($type, $message, $data);
    }

    /**
     * Mark logs for a specific mail
     * @param $mailId
     * @param $logs
     */
    public function addLogsForMail($mailId, $logs)
    {
        foreach($logs as &$log)
        {
            if(!isset($log['data'])) {
                $log['$data'] = [];
            }
            $log['$data']['$mail']  = $mailId;
        }
        $this->addLogs($logs);
    }

    /**
     * Get logs for a specific mail
     * @param  $mailId
     * @return array
     */
    public function getLogsForMail($mailId)
    {
        return array_filter($this->getLogs(), function($log) use ($mailId) {
            return isset($log['$data']) && ($log['$data']['$mail'] === $mailId);
        });
    }

}