<?php

namespace Neneff\Import\Excel;


use Neneff\Tools\Log;
use Neneff\Tools\Profiler;
use Neneff\Tools\Sql;
use Neneff\Tools\Variable;
use PhpOffice\PhpSpreadsheet\Shared\Date;


abstract class AbstractImportExcelTable extends AbstractImportExcel
{
    /** @var Integer | null <p>null will Autoselect </p>*/
    protected $_lastColumn = null;

    /** @var array[] <p>Columns that should exists in the line rows (not in order tho)</p> */
    protected $_mandatoryColumns = [];

    /** @var string <p>table linked in your DB to be match with</p> */
    protected $_table = null;

    /** @var array  */
    protected $_tableFields = [];

    /** @var array <p> assoc between field name and types</p>*/
    protected $_fieldCasts = [];

    /**
     * {@inheritdoc}
     */
    public function __construct(\PDO $pdo, $path)
    {
        // -- Get table description to fill the existing columns, to be match with the first header line to know how to insert them in the import process
        $this->_tableFields = array_map(
            function($row) {
                return $row['Field'];
            },
            $pdo->query("DESCRIBE {$this->_table}")->fetchAll(\PDO::FETCH_ASSOC)
        );

        $this->addLog(Log::$LOG_DEBUG, 'before construct - memory_get_usage: '.Profiler::byteToMbyte(memory_get_usage(true)));

        parent::__construct($pdo, $path);

        $this->addLog(Log::$LOG_DEBUG, 'after construct - memory_get_usage: '.Profiler::byteToMbyte(memory_get_usage(true)));

    }


    /**
     * @param   array  $row
     *
     * @return  array
     * @throws \Exception
     */
    protected function _processRow($row)
    {
        // -- cast fields
        foreach($this->_fieldCasts as $fieldName => $type)
        {
            $key = $this->_resolverKeyForField($fieldName);

            if(isset($row[$key]))
            {
                switch($type)
                {
                    case 'int':
                        $row[$key] = ($row[$key] !== null) ? intval($row[$key]) : null;
                        break;

                    case 'float':
                        $row[$key] = ($row[$key] !== null) ? floatval($row[$key]) : null;
                        break;

                    case 'datetime':
                        $row[$key] = $row[$key] ? Date::excelToDateTimeObject($row[$key])->format('Y-m-d H:i:s') : null;
                        break;

                    case 'date':
                        $row[$key] = $row[$key] ? Date::excelToDateTimeObject($row[$key])->format('Y-m-d') : null;
                        break;
                }
            }
        }

        // -- complete the rows with missing lines
        $completeRow = [];
        foreach($this->_tableFields as $tableFieldName)
        {
            $completeRow[] = Variable::issetOrNull($row[$this->_resolverKeyForField($tableFieldName)]);
        }
        return $completeRow;
    }


    /**
     * @return string
     */
    public function getFinalTable()
    {
        return $this->_table;
    }


    /**
     * @return string
     */
    public function getImportTable()
    {
        return "_import_{$this->_table}";
    }


    /**
     * when start import process
     */
    final protected function _importInit()
    {
        $this->addLog(Log::$LOG_MESSAGE, "Importing data in temporary table");

        // -- Reset temporary table
        $this->_pdo->exec("DROP TABLE IF EXISTS {$this->getImportTable()} ");
        $this->_pdo->exec("CREATE TABLE {$this->getImportTable()} LIKE {$this->_table} ");

        $this->addLog(Log::$LOG_DEBUG, 'before import - memory_get_usage: '. Profiler::byteToMbyte(memory_get_usage(true)));
    }

    /**
     * Execute the import
     * You should import your data into you tool from here
     *
     * @param array $chunk <p>Chunk of data to be imported</p>
     * @throws
     */
    final protected function _importChunk($chunk)
    {
        $valueCount = count($chunk);

        if($valueCount > 0)
        {
            $placeholders = Sql::preparePlaceholdersList($valueCount, count($this->_tableFields));
            $stmt         = $this->_pdo->prepare("
                INSERT INTO {$this->getImportTable()}
                VALUES {$placeholders}
            ");
            $stmt->execute($chunk);
        }
    }

    /**
     * @throws \Exception
     */
    final protected function _importEnd()
    {
        $this->addLog(Log::$LOG_DEBUG, 'end import - memory_get_usage: '.Profiler::byteToMbyte(memory_get_usage(true)));

        // -- post-process to be performed
        $this->addLog(Log::$LOG_MESSAGE, "Applying post process");
        $this->_performPostProcess();

        $this->addLog(Log::$LOG_MESSAGE, "Committing new data");
        $this->_commitNewData();

        $this->addLog(Log::$LOG_DEBUG, 'after commit - memory_get_usage: '.Profiler::byteToMbyte(memory_get_usage(true)));
        $this->addLog(Log::$LOG_DEBUG, 'after commit - memory_get_peak_usage: '.Profiler::byteToMbyte(memory_get_peak_usage(true)));
    }

    /**
     * Once the temporary table is set, perform post process on it before the temporary table is copied in the final table
     */
    protected function _performPostProcess()
    {

    }


    /**
     * Once the temporary table is set, perform post process on it before it will be copied in the final table
     * @throws \Exception
     */
    protected function _commitNewData()
    {
        try
        {
            $nbPreviousData = $this->_pdo->query("SELECT COUNT(*) FROM {$this->_table} ")->fetchColumn();
            $this->_pdo->beginTransaction();

            // -- Once done, we can copy this temp table to the final one.
            $this->_pdo->exec("TRUNCATE {$this->_table}");
            $this->_pdo->exec("INSERT INTO {$this->_table} SELECT * FROM {$this->getImportTable()}");

            $this->_pdo->commit();
            $nbCurrentData = $this->_pdo->query("SELECT COUNT(*) FROM {$this->_table} ")->fetchColumn();

            $this->addLog(Log::$LOG_MESSAGE, "Importation finished, \"{$this->_table}\" table have now {$nbCurrentData} rows (previous was {$nbPreviousData}) rows");
        }
        catch(\Exception $e)
        {
            $this->_pdo->rollBack();
            $this->addLog(Log::$LOG_ERROR, 'An error occurred during the insertion of the new data', [
                '$exception' => [
                    'code'    => $e->getCode(),
                    'message' => $e->getMessage(),
                    'file'    => $e->getFile(),
                    'line'    => $e->getLine(),
                    'trace'   => $e->getTraceAsString(),
                ]
            ]);

            throw $e;
        }

    }


    /**
     * @param  String $fieldName
     * @return int | null
     */
    protected function _resolverKeyForField($fieldName)
    {
        $result = array_search($fieldName, $this->_extractedHeader[0]);
        return is_int($result) ? $result : null;
    }


    /**
     * check template header with the current header you extract from the file you import
     * @param  array   $extractedHeaders
     * @return Boolean
     * @throws \Exception
     */
    protected function _validateTemplateHeader(array $extractedHeaders)
    {
        if(count($extractedHeaders) !== 1) {
            throw new \Exception("This import suppose to have one row of header to mark fields, verify your import configuration");
        }

        foreach($this->_mandatoryColumns as $mandatoryColumn)
        {
            if(!in_array($mandatoryColumn, $extractedHeaders[0]))
            {
                throw new \Exception("Mandatory fields \"{$mandatoryColumn}\" has not been found in the import Excel file, check the file headers");
            }
        }
        return true;
    }


}